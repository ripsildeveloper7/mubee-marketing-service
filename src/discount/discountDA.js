var discountDetails = require('../model/discount.model');

exports.createDiscount = function(req, dateString, res) {
    var create = new discountDetails(req.body);
    create.creationDate = dateString;
    create.save(function(err, saveData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}
exports.getDiscount = function(req, res) {
    discountDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.deleteSingleDiscount = function(req, res) {
    discountDetails.findOneAndRemove({'_id': req.params.id}).exec(function(err, deleteData){
        if(err) {
            res.status(500).json(err);
        } else {
            discountDetails.find({}).select().exec(function(err, findData) {
                if (err) {
                   res.status(500).json(err); 
                } else {
                    res.status(200).json(findData);
                }
            })
        }
    })
}