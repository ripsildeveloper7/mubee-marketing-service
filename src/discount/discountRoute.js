var discountMgr = require('./discountMgr');

module.exports = function (app) {
    app.route('/creatediscount').post(discountMgr.createDiscount);
    app.route('/getdiscount').get(discountMgr.getDiscount);
    app.route('/deletesinglediscount/:id').delete(discountMgr.deleteSingleDiscount);
}