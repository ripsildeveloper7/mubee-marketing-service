var discountDA = require('./discountDA');

exports.createDiscount = function(req, res) {
    try {
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var dateString = month + "/" + date + "/" + year;
        discountDA.createDiscount(req, dateString, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getDiscount = function(req, res) {
    try {
        discountDA.getDiscount(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteSingleDiscount = function(req, res) {
    try {
        discountDA.deleteSingleDiscount(req, res);
    } catch (error) {
        console.log(error);
    }
}