var mongoose = require('mongoose');
var couponCondition = require('./condition.model');

const FlexibleCouponSchema = new mongoose.Schema({
    couponCode: String,
    description: String,
    amountType: String,
    typeValue: Number,
    conditions: [couponCondition],
    couponStatus: Boolean,
    advanceSettings: Boolean,
    startDate: Date,
    endDate: Date,
    creationDate: Date,
    modificationDate: Date,
    maximumUsage: Number
});
const coupon = mongoose.model('flexiblecoupon', FlexibleCouponSchema);
module.exports = coupon;