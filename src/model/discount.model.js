var mongoose = require('mongoose');
var discountCondition = require('./discountCondition.model');

const DiscountSchema = new mongoose.Schema({
    name: String,
    ruleType: String,
    amountType: String,
    typeValue: Number,
    applyOn: String,
    conditions: [discountCondition],
    discountStatus: Boolean,
    advanceSettings: Boolean,
    startDate: Date,
    endDate: Date,
    creationDate: Date
});
const Discount = mongoose.model('discount', DiscountSchema);
module.exports = Discount;