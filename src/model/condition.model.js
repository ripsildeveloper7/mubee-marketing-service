var mongoose = require('mongoose');

const ConditionSchema = new mongoose.Schema({
    field: String,
    operator: String,
    value: [String],
});
module.exports = ConditionSchema;