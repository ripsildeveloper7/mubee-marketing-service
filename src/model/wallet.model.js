var mongoose = require('mongoose');

const WalletSchema = new mongoose.Schema({
    minCoin: Number,
    maxCoin: Number,
    loyaltyWallet: [{
        fixedAmount: Number,
        fixedCoin: Number,
        usingPercentage: Number,
        minimunCondition: Boolean,
        minimumOrder: Number,
        maximumCondition: Boolean,
        maximumOrder: Number
    }],
    activityWallet: [{
        addingImage: Number,
        addingVideo: Number,
        firstTimeRegistration: Number,
        usingPercentage: Number,
        minimunCondition: Boolean,
        minimumOrder: Number,
        maximumCondition: Boolean,
        maximumOrder: Number
    }],
    referWallet: [{
        usingPercentage: Number,
        minimunCondition: Boolean,
        minimumOrder: Number,
        maximumCondition: Boolean,
        maximumOrder: Number
    }],
    eventWallet: [{
        usingPercentage: Number,
        minimunCondition: Boolean,
        minimumOrder: Number,
        maximumCondition: Boolean,
        maximumOrder: Number
    }],
    addedDate: Date,
    modificationDate: Date,
    vaildationPeriodForCoin: Number
});
const Wallet = mongoose.model('wallet', WalletSchema);
module.exports = Wallet;