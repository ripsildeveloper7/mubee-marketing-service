var walletDA = require('./walletDA');

exports.addWalletByAdmin = function(req, res) {
    try {
        walletDA.addWalletByAdmin(req, res);
    } catch(error) {
        console.log(error);
    }
    
}
exports.getWallet = function(req, res) {
    try {
        walletDA.getWallet(req, res);
    } catch(error) {
        console.log(error);
    }
    
}