var walletMgr = require('./walletMgr');

module.exports = function (app) {
    app.route('/addwalletbyadmin').post(walletMgr.addWalletByAdmin);
    app.route('/getwalletForadmin').get(walletMgr.getWallet);
}