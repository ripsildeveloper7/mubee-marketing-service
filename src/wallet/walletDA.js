var WalletModel = require('../model/wallet.model');

exports.addWalletByAdmin = function(req, res) {
    WalletModel.find({}).select().exec(function(err, wallet) {
        if (err) {
            res.status(500).josn(err);
        } else {
            if (wallet.length === 0) {
                var create = new WalletModel(req.body);
                create.addedDate = Date.now();
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                wallet[0].minCoin = req.body.minCoin;
                wallet[0].maxCoin = req.body.maxCoin;
                wallet[0].modificationDate = Date.now();
                wallet[0].loyaltyWallet = req.body.loyaltyWallet;
                wallet[0].activityWallet = req.body.activityWallet;
                wallet[0].referWallet = req.body.referWallet;
                wallet[0].eventWallet = req.body.eventWallet;
                wallet[0].vaildationPeriodForCoin = req.body.vaildationPeriodForCoin;
                wallet[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}

exports.getWallet = function(req, res) {
    WalletModel.find({}).select().exec(function(err, wallet) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(wallet);
        }
    })
}