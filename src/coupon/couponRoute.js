var couponMgr = require('./couponMgr');

module.exports = function(app) {
    app.route('/createfirsttimecoupon').post(couponMgr.createFirstTimeCoupon);
    app.route('/getfirsttimecoupon').get(couponMgr.getFirstTimeCoupon);
    // app.route('/checkcoupon').post(couponMgr.checkFirstTimeCoupon);
    app.route('/createflexiblecoupon').post(couponMgr.createFlexibeCoupon);
    app.route('/getflexiblecoupon').get(couponMgr.getFlexibleCoupon);
    app.route('/getsingleflexiblecoupon/:id').get(couponMgr.getSigleFlexibleCoupon);
    app.route('/updateflexiblecoupon/:id').put(couponMgr.updateFlexibleCoupon);
    app.route('/deleteflexiblecoupon/:id').delete(couponMgr.deleteFlexibleCoupon);
    app.route('/getfirstsignupcouponforproduct').get(couponMgr.getFirstTimeCouponForProduct);
    app.route('/getflexiblecouponforproduct').get(couponMgr.getFlexibleCouponForProduct);
    app.route('/checkcouponforplaceorder/:id').get(couponMgr.checkCouponForPlaceOrder);
}