var couponDA = require('./couponDA');

exports.createFirstTimeCoupon = function(req, res) {
    try {
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var dateString = month + "/" + date + "/" + year;
        couponDA.createFirstTimeCoupon(req, res, dateString);
    } catch (error) {
        console.log(error);
    }
}
exports.getFirstTimeCoupon = function(req, res) {
    try {
        couponDA.getFirstTimeCoupon(req, res);
    } catch (error) {
        console.log(error);
    }
}

// exports.checkFirstTimeCoupon = function(req, res) {
//     try {
//         couponDA.checkFirstTimeCoupon(req, res);
//     } catch (error) {
//         console.log(error);
//     }
// }
exports.getFirstTimeCouponForProduct = function(req, res) {
    try {
        couponDA.getFirstTimeCouponForProduct(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getFlexibleCouponForProduct = function(req, res) {
    try {
        couponDA.getFlexibleCouponForProduct(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.createFlexibeCoupon = function(req, res) {
    try {
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var dateString = month + "/" + date + "/" + year;
        couponDA.createFlexibeCoupon(req, res, dateString);
    } catch (error) {
        console.log(error);
    }
}
exports.getFlexibleCoupon = function(req, res) {
    try {
        couponDA.getFlexibleCoupon(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSigleFlexibleCoupon = function(req, res) {
    try {
        couponDA.getSigleFlexibleCoupon(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updateFlexibleCoupon = function(req, res) {
    try {
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var dateString = month + "/" + date + "/" + year;
        couponDA.updateFlexibleCoupon(req, res, dateString);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteFlexibleCoupon = function(req, res) {
    try {
        couponDA.deleteFlexibleCoupon(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.checkCouponForPlaceOrder = function(req, res) {
    try {
        couponDA.checkCouponForPlaceOrder(req, res);
    } catch (error) {
        console.log(error);
    }
}
