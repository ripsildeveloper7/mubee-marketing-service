var firstTimeCoupon = require('../model/firstTimeCoupon.model');
var flexibleCoupon = require('../model/flexible.model');

exports.createFirstTimeCoupon = function (req, res, date) {
    firstTimeCoupon.find({}).select().exec(function (err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (coupon.length === 0) {
                var create = new firstTimeCoupon(req.body);
                create.creationDate = date;
                create.save(function (err, coupon) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(coupon);
                    }
                })
            } else {
                coupon[0].couponCode = req.body.couponCode;
                coupon[0].description = req.body.description;
                coupon[0].amountType = req.body.amountType;
                coupon[0].typeValue = req.body.typeValue;
                coupon[0].couponStatus = req.body.couponStatus;
                coupon[0].advanceSettings = req.body.advanceSettings;
                coupon[0].startDate = req.body.startDate;
                coupon[0].endDate = req.body.endDate;
                coupon[0].modificationDate = date;
                coupon[0].maximumUsage = req.body.maximumUsage;
                coupon[0].conditions = req.body.conditions;
                coupon[0].save(function(err, saveData)  {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }
        }
    })
}
exports.getFirstTimeCoupon = function (req, res) {
    firstTimeCoupon.find({}).select().exec(function (err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(coupon);
        }
    })
}
// exports.checkFirstTimeCoupon = function(req, res) {
//     firstTimeCoupon.find({'couponCode': req.body.couponCode}).select().exec(function(err, coupon) {
//         if (err) {
//             res.status(500).json(err);
//         } else {
//             const currentDate = new Date();
//             if (coupon.length === 0) { 
//                 /* res.status(200).json(coupon); */
//                 flexibleCoupon.find({'couponCode': req.body.couponCode}).select().exec(function(err, flexCoupon) {
//                     if (err) {
//                         res.status(500).json(err);
//                     } else {
//                         if (flexCoupon.length === 0) {
//                             res.status(200).json(flexCoupon);
//                         } else {
//                             if (flexCoupon[0].couponStatus === true) {
//                                 if (flexCoupon[0].advanceSettings === true) {
//                                     if (flexCoupon[0].maximumUsage !== 0 && flexCoupon[0].maximumUsage !== undefined && flexCoupon[0].maximumUsage !== null) {
//                                     if (flexCoupon[0].startDate <= currentDate && flexCoupon[0].endDate >= currentDate) {
//                                         res.status(200).json(flexCoupon);
//                                     } else {
//                                         res.status(200).send({
//                                             "result": 'InValid'
//                                         });
//                                     }
//                                 } else {
//                                     res.status(200).send({
//                                         "result": 'InValid'
//                                     });
//                                 }
//                                 } else {
//                                     res.status(200).json(flexCoupon);
//                                 }
//                             } else {
//                                 res.status(200).send({
//                                     "result": 'InValid'
//                                 });
//                             }
//                         }
//                     }
//                 })
//             } else {
//                 if (coupon[0].couponStatus === true) {
//                     if (coupon[0].advanceSettings === true) {
//                         if (coupon[0].maximumUsage !== 0 && coupon[0].maximumUsage !== undefined && coupon[0].maximumUsage !== null) {
//                         if (coupon[0].startDate <= currentDate && coupon[0].endDate >= currentDate) {
//                             res.status(200).json(coupon);
//                         } else {
//                             res.status(200).send({
//                                 "result": 'InValid'
//                             });
//                         }
//                     } else {
//                         res.status(200).send({
//                             "result": 'InValid'
//                         });
//                     }
//                     } else {
//                         res.status(200).json(coupon);
//                     }
//                 } else {
//                     res.status(200).send({
//                         "result": 'InValid'
//                     });
//                 }
//             }
//         }
//     })
// }
exports.getFirstTimeCouponForProduct = function(req, res) {
    firstTimeCoupon.find({}).select().exec(function(err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            const currentDate = new Date();
            if (coupon.length === 0) {
                res.status(200).json(coupon);
            } else {
            if (coupon[0].couponStatus === true) {
                if (coupon[0].advanceSettings === true) {
                    if (coupon[0].maximumUsage !== 0 && coupon[0].maximumUsage !== undefined && coupon[0].maximumUsage !== null) {
                    if (coupon[0].startDate <= currentDate && coupon[0].endDate >= currentDate) {
                        res.status(200).json(coupon);
                    } else {
                        res.status(200).json(coupon)
                    }
                } else {
                    res.status(200).json(coupon)
                }
                } else {
                    res.status(200).json(coupon);
                }
            } else {
                res.status(200).json(coupon)
            }
         }
        }
    })
}
exports.getFlexibleCouponForProduct = function(req, res) {
    flexibleCoupon.find({couponStatus: true}).select().exec(function(err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (coupon.length === 0) {
                res.status(200).json(coupon);
            } else {
                const array = [];
                const currentDate = new Date();
                coupon.forEach(element => {
                    if (element.advanceSettings === true) {
                        if (element.maximumUsage !== 0 && element.maximumUsage !== undefined && element.maximumUsage !== null) {
                            if (element.startDate <= currentDate && element.endDate >= currentDate) {
                                array.push(element);
                            }
                        } 
                    } else {
                        array.push(element);
                    }
                })
                res.status(200).json(array);
            }
        }
    })
}
exports.createFlexibeCoupon = function(req, res, date) {
    var create = new flexibleCoupon(req.body);
    create.creationDate = date;
    create.save(function(err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(coupon);
        }
    })
}
exports.getFlexibleCoupon = function (req, res) {
    flexibleCoupon.find({}).select().exec(function (err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(coupon);
        }
    })
}
exports.getSigleFlexibleCoupon = function (req, res) {
    flexibleCoupon.findOne({'_id': req.params.id}).select().exec(function (err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(coupon);
        }
    })
}
exports.updateFlexibleCoupon = function( req, res, date) {
    flexibleCoupon.findOne({'_id': req.params.id}).select().exec(function(err, coupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            coupon.couponCode = req.body.couponCode;
            coupon.description = req.body.description;
            coupon.amountType = req.body.amountType;
            coupon.typeValue = req.body.typeValue;
            coupon.couponStatus = req.body.couponStatus;
            coupon.advanceSettings = req.body.advanceSettings;
            coupon.startDate = req.body.startDate;
            coupon.endDate = req.body.endDate;
            coupon.modificationDate = date;
            coupon.maximumUsage = req.body.maximumUsage;
            coupon.conditions = req.body.conditions;
            coupon.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.deleteFlexibleCoupon = function(req, res) {
    flexibleCoupon.findOneAndRemove({'_id': req.params.id}).select().exec(function(err, deleteData) {
        if (err) {
            res.status(500).json(err);
        } else {
            flexibleCoupon.find({}).select().exec(function (err, coupon) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(coupon);
                }
            })
        }
    })
}
exports.checkCouponForPlaceOrder = function(req, res) {
    firstTimeCoupon.find({'_id': req.params.id}).select().exec(function(err, first) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (first.length === 0) {
                flexibleCoupon.find({'_id': req.params.id}).select().exec(function(err, flexible) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(flexible);
                    }
                })
            } else {
                res.status(200).json(first);
            }
        }
    })
}