var discountRoute = require('./discount/discountRoute');
var couponRoute = require('./coupon/couponRoute');
var walletRoute = require('./wallet/walletRoute');

exports.loadRoutes = function (app) {
    discountRoute(app);
    couponRoute(app);
    walletRoute(app);
}